<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap Final Crud Validation System</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <h3>PHP CRUD Grid</h3>
        </div>
        <div class="row">
            <p><a class="btn btn-xs btn-success" href="create.php">Create</a></p>
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Action</th>
                </tr>
                <tbody>
                <?php 
                    include 'lib/db.php';
                    $sql = "SELECT * FROM tb_crud ORDER BY id DESC";
                    $sth = $dbh->prepare($sql);
                    $sth->execute();
                    $result = $sth->fetchAll(PDO::FETCH_ASSOC);
					//echo json_encode($result);
                    foreach ($result as $row) {?>
                      <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['fname'].' '.$row['lname']; ?></td>
                        <td><?php echo $row['age']; ?></td>
                        <td><?php echo $row['gender']; ?></td>
                        <td>
                          <a class="btn btn-xs btn-info" href="read.php?id=<?php echo $row['id']; ?>">Read</a>
                          <a class="btn btn-xs btn-primary" href="update.php?id=<?php echo $row['id']; ?>">Update</a>
                          <a class="btn btn-xs btn-danger" href="delete.php?id=<?php echo $row['id']; ?>">Delete</a>
                        </td>
                      </tr>                         
                    <?php  
                    }
                ?>           
                </tbody>
            </table>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
