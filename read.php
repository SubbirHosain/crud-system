<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap Final Crud Validation System</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="col-sm-12">
            <div class="row">
                <h3>Read a User</h3>
            </div>
            <?php 
                include 'lib/db.php';
                if (isset($_GET['id'])) {
                    $page_id = $_GET['id'];
                    $sql =  "SELECT * FROM tb_crud WHERE id = ?";
                    $sth = $dbh->prepare($sql);
                    $sth->execute(array($page_id));

                    //for single data collection it is better to use
                    $row = $sth->fetch(PDO::FETCH_ASSOC); //single array return 

                    //If you use fetchAll a loop needs to be used 
                    //returns associative array
                    /*$result = $sth->fetchAll(PDO::FETCH_ASSOC);                  
                    var_dump($result);
                    foreach ($result as $row) {
                        echo $row['fname'];
                    }*/
                }else{
                    header("location:index.php");
                }

            ?>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $row['fname']; ?></p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $row['lname']; ?></p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Age</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $row['age']; ?></p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <label class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $row['gender']; ?></p>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <a class="btn btn btn-default" href="index.php">Back</a>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
