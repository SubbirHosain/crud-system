<?php 

$host = "localhost";
$user = "root";
$pass ="root";
$database ="final_crud";

//custom PDO options
//
$options = array(

    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_EMULATE_PREPARES => false

    );
try{

    $dbh = new PDO("mysql:host=$host;dbname=$database",$user,$pass,$options);
}
catch(PDOException $e){
    echo "Connection error: ".$e->getMessage();
}