<?php 
require 'lib/db.php';

//define error variables set either null or empty values
$fnameError = '';
$lnameError = '';
$ageError = '';
$genderError = '';

//defining variables to show data on form after submitting form with errors
$fname = '';
$lname = '';
$age = '';
$gender = '';

//sanitize all form fields
function validate_input($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

//imagine everything is valid means there is no error
$valid = true;

//Checking if the form is submitted or not
if (isset($_GET['form_submit'])) {
    
    //Checking fname field 
    if (empty($_GET['fname'])) {
        $fnameError = "Please enter First Name";
        $valid = false;
    }else{
        $fname = validate_input($_GET['fname']);
        if (strlen($fname)<3) {
            $fnameError = "The name must be at least 3 characters longer";
        }
        if (!preg_match("/^[a-zA-Z ]*$/", $fname)) {
            $fnameError = "Only letters and white space allowed";
        }
    }

    //checking lname field
    if (empty($_GET['lname'])) {
        $lnameError = "Please enter Last Name";
        $valid = false;
    }else{
        $lname = validate_input($_GET['lname']);
        if (strlen($lname)<3) {
            $lnameError = "The name must be at least 3 characters longer";
        }
        else if (!preg_match("/^[a-zA-Z ]*$/", $lname)) {
            $lnameError = "Only letters and white space allowed";
        }
    }

    //checking age field
    if (empty($_GET['age'])) {
        $ageError = "Please Your age is reuired";
        $valid = false;
    }
    else{
        $age = validate_input($_GET['age']);
        if (!preg_match("/^[0-9]*$/", $lname)) {
            $ageError = "Only digits are allowed";
        }
    }

    //checking gender field
    if (empty($_GET['gender'])) {
        $ageError = "Please Your gender is reuired";
        $valid = false;
    }
    else{
        $gender = validate_input($_GET['gender']);
    }

    //if everything is ok 
    if ($valid) {
        $sql = "INSERT INTO tb_crud (fname,lname,age,gender) VALUES (?,?,?,?)";
        $sth = $dbh->prepare($sql);
        $data = array($fname,$lname,$age,$gender);
        $sth->execute($data);
        echo '<h1>Thank You</h1>';
        
    }
    else{
        echo "wrong something";
        echo $fnameError; 
        echo $lnameError; 
       
    }

}
else
{
    echo 'come after submitting the form';
}