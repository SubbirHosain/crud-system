<?php 
require 'lib/db.php';

//define error variables set either null or empty values
$fnameError = '';
$lnameError = '';
$ageError = '';
$genderError = '';

//defining variables to show data on form after submitting form with errors
$fname = '';
$lname = '';
$age = '';
$gender = '';

//sanitize all form fields
function validate_input($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

//imagine everything is valid means there is no error
$valid = true;  
//Checking if the form is submitted or not
if (isset($_GET['form_submit'])) {
    
    //Checking fname field 
    if (empty($_GET['fname'])) {
        $fnameError = "Please enter First Name";
        $valid = false;
    }else{
        $fname = validate_input($_GET['fname']);
        if (strlen($fname)<3) {
            $fnameError = "The name must be at least 3 characters longer";
            $valid = false;
        }
        else if (!preg_match("/^[a-zA-Z ]*$/", $fname)) {
            $fnameError = "Only letters and white space allowed";
            $valid = false;
        }
    }

    //checking lname field
    if (empty($_GET['lname'])) {
        $lnameError = "Please enter Last Name";
        $valid = false;
    }else{
        $lname = validate_input($_GET['lname']);
        if (strlen($lname)<3) {
            $lnameError = "The name must be at least 3 characters longer";
            $valid = false;
        }
        else if (!preg_match("/^[a-zA-Z ]*$/", $lname)) {
            $lnameError = "Only letters and white space allowed";
            $valid = false;
        }
    }

    //checking age field
    if (empty($_GET['age'])) {
        $ageError = "Please Your age is reuired";
        $valid = false;
    }
    else{
        $age = validate_input($_GET['age']);
        if (!preg_match("/^[0-9]*$/", $age)) {
            $ageError = "Only digits are allowed";
            $valid = false;
        }
    }

    //checking gender field
    if (empty($_GET['gender'])) {
        $genderError = "Please Your gender is reuired";
        $valid = false;
    }
    else{
        $gender = validate_input($_GET['gender']);
    }

    //if everything is ok 
    if ($valid) {
        $sql = "INSERT INTO tb_crud (fname,lname,age,gender) VALUES (?,?,?,?)";
        $sth = $dbh->prepare($sql);
        $data = array($fname,$lname,$age,$gender);
        $status = $sth->execute($data);
        if($status){
			header("location:index.php");
		}
        
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap Final Crud Validation System</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <h3>Create a User</h3>
        </div>
        <div class="row">
            <form method="GET" action="">

                <div class="form-group <?php echo !empty($fnameError)?'has-error':'';?>">
                    <label for="inputFName">First Name</label>
                    <input type="text" class="form-control" value="<?php echo !empty($fname)?$fname:'';?>" name="fname" placeholder="First Name">
                    <span class="help-block"><?php echo !empty($fnameError)?$fnameError:'' ;?></span>
                </div>

                <div class="form-group <?php echo !empty($lnameError)?'has-error':''; ?>">
                    <label for="inputLName">Last Name</label>
                    <input type="text" class="form-control"   value="<?php echo !empty($lname)?$lname:'';?>" name="lname" placeholder="Last Name">
                    <span class="help-block"><?php echo !empty($lnameError)?$lnameError:'' ;?></span>
                </div>

                <div class="form-group <?php echo !empty($ageError)?'has-error':'';?>">
                    <label for="inputAge">Age</label>
                    <input type="number"  class="form-control"  value="<?php echo !empty($age)?$age:'';?>" name="age" placeholder="Age">
                    <span class="help-block"><?php echo !empty($ageError)?$ageError:'' ;?></span>
                </div>

                <div class="form-group <?php echo !empty($genderError)?'has-error':'';?>">
                    <label for="inputGender">Gender</label>
                    <select class="form-control"  name="gender">
                        <option value=""></option>
                        <option value="Male" <?php if (isset($_GET['gender']) && $gender=="Male") {
                            echo "selected="."\"selected\"";
                        } ?> >Male</option>
                        <option value="Female" <?php if (isset($_GET['gender']) && $gender=="Female") {
                            echo "selected="."\"selected\"";
                        } ?> >Female</option>
                    </select>
                    <span class="help-block"><?php echo !empty($genderError)?$genderError:'' ;?></span>
                </div>

                <div class="form-actions">
                    <button type="submit" name="form_submit" class="btn btn-success">Create</button>
                    <a class="btn btn btn-default" href="index.php">Back</a>
                </div>
            </form>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
