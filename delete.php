<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap Final Crud Validation System</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="row">
                <h3>Delete a User</h3>
            </div>
            <?php 
                include 'lib/db.php';
                if(isset($_REQUEST['id'])){

                    $id = $_REQUEST['id'];
                    //delete data with id
                    //what if i put my query on this page? 
                }
                else{
                    header('location:index.php');
                }                
                if (isset($_GET['form_delete'])) {
                    $sql = "DELETE FROM tb_crud WHERE id=?";
                    $sth=$dbh->prepare($sql);
                    $sth->execute(array($id));
                    header('location:index.php');                    
                }
            ?>
            <form method="GET" action="">
                <input type="hidden" name="id" value="<?php echo $id;?>" />
                <p class="bg-danger" style="padding: 10px;">Are you sure to delete ?</p>
                <div class="form-actions">
                    <button type="submit" name="form_delete" class="btn btn-danger">Yes</button>
                    <a class="btn btn btn-default" href="index.php">No</a>
                </div>
            </form>
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>
