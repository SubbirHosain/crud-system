-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2017 at 06:53 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_crud`
--

DROP TABLE IF EXISTS `tb_crud`;
CREATE TABLE IF NOT EXISTS `tb_crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `age` int(3) NOT NULL,
  `gender` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_crud`
--

INSERT INTO `tb_crud` (`id`, `fname`, `lname`, `age`, `gender`) VALUES
(22, 'Subbir', 'Hosain', 55, 'Male'),
(21, 'Subbir', 'Hosain', 44, 'Male'),
(20, 'Subbirda', 'Hosain', 44, 'male'),
(19, 'Subbir', 'Hosain', 33, 'male'),
(18, 'Subbire', 'Hosain', 25, 'Male'),
(17, 'Subbir', 'Hosain', 25, 'Male'),
(9, 'Musa', 'Hosain', 34, 'Male'),
(14, 'Subbir', 'Hosain', 32, 'Male'),
(15, 'Subbir', 'Hosain', 32, 'Male'),
(16, 'Subbird', 'Hosaine', 45, 'Male');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
